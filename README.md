# Yii2 Dadata&Cache

## Description!
Standatr api and cache

## Install
```
php composer.phar require rocknroi/dadata-yii2
```

## Usage
```
$client = new Dadata\Client(new \GuzzleHttp\Client(), [
    'token' => '...',
    'secret' => '...',
]);
```
## Clean
```
$response = $client->cleanAddress('мск сухонска 11/-89');
$response = $client->cleanPhone('тел 7165219 доб139');
$response = $client->cleanPassport('4509 235857');
$response = $client->cleanName('Срегей владимерович иванов');
$response = $client->cleanEmail('serega@yandex/ru');
$response = $client->cleanDate('24/3/12');
$response = $client->cleanVehicle('форд фокус')
```